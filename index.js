/** 
 *
 *    *  .  . *       *    .        .        .   *    ..
 .    *        .   ###     .      .        .            *
    *.   *        #####   .     *      *        *    .
  ____       *  ######### *    .  *      .        .  *   .
 /   /\  .     ###\#|#/###   ..    *    .      *  .  ..  *
/___/  ^8/      ###\|/###  *    *            .      *   *
|   ||%%(        # }|{  #
|___|,       MAKE IT AWESOME    }|{
 * 
 */

window.onload = init()


function init() {
	const data = getData();
	console.log(data)

}

function getData(message) {
	return [{
		firstName: "Jacob",
		lastName: "Hawkins",
		job: "Senior Editor",
		location:"Senior Editor",
		profile: "./imgs/jacob-hawkins.png"
	},
	{
		firstName: "Esther",
		lastName:"Black",
		job: "Editor in Chief",
		location:"Wall Street Journal",
		profile: "./imgs/esther-black.png"
	},
	{
		firstName: "Philip",
		lastName:"Miles",
		job: "Senior Writer",
		location:"Washington Post",
		profile: "./imgs/philip-miles.png"
	}]
}